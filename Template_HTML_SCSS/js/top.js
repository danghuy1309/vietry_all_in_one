// DOCUMENT READY
jQuery(document).ready(function() {
    "use strict";

});


// WINDOW LOAD
jQuery(window).bind('load', function() {
    "use strict";
    // SLIDER
    var slider_ID = '#slider-id';
    if( jQuery(slider_ID).length > 0 ) {
        jQuery(slider_ID).slick({
            dots: false,
            infinite: true,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            arrows: false,
            centerMode: false,
            centerPadding: 0,
            pauseOnHover: false,
            fade: false,
            variableWidth: false,
        });
        var _length_items = jQuery('#slider-id .slider-item').length;
        jQuery(slider_ID).on('beforeChange', function(event, slick, currentSlide, nextSlide){
            if(currentSlide === (_length_items - 1)) {
                jQuery('#slider-id .slick-current + .slick-slide').addClass('cloned-active');
            }
            if(currentSlide === 0) {
                jQuery('#slider-id [data-slick-index="-1"]').addClass('cloned-active');
            }
        });
        jQuery(slider_ID).on('afterChange', function(event, slick, currentSlide){
            jQuery('#slider-id .cloned-active').removeClass('cloned-active');
        });
    }
    /*============== END - SLIDER ================*/

    // CONTENT LOAD ANIMATION
    AOS.init({
        duration: 600,
        disable: 'mobile',
        once: true
    });
    /*============== END - CONTENT LOAD ANIMATION ================*/
});